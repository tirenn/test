package main

import (
	"log"
	"net/http"
	"os"

	c "test/config"
	user "test/user"

	"github.com/julienschmidt/httprouter"
)

var Config *c.ConfigType

func init() {
	Config = c.Load()
	user.Load(Config)
}

func main() {
	router := httprouter.New()

	router.GET("/ping", ping)
	router.POST("/signup", user.Create)
	// router.POST("/login", user.Login)
	// router.GET("/user", user.Get)
	// router.GET("/user/:id", user.GetbyId)
	// router.DELETE("/user/:id", user.Delete)

	port := os.Getenv("PORT")
	log.Fatal(http.ListenAndServe(port, router))
}

func ping(w http.ResponseWriter, r *http.Request, params httprouter.Params) {
	w.Write([]byte("Hello"))
}
