package config

import (
	"database/sql"
)

type ConfigType struct {
	DB *sql.DB
}
