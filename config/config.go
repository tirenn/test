package config

import (
	"fmt"
	mysql "test/mysql"
)

var config ConfigType

func Load() *ConfigType {
	db, err := mysql.Connect()
	if err != nil {
		fmt.Println(err.Error())
	}

	config.DB = db
	// defer db.Close()

	return &config
}
