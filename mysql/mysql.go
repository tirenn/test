package mysql

import (
	"database/sql"
	"os"

	_ "github.com/go-sql-driver/mysql"
)

func Connect() (*sql.DB, error) {
	dbType := os.Getenv("DB_TYPE")
	dbUrl := os.Getenv("DB_URL")

	db, err := sql.Open(dbType, dbUrl)
	if err != nil {
		return nil, err
	}

	return db, nil
}
