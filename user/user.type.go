package user

type User struct {
	ID          string `bson:"id" json:"id"`
	Username    string `bson:"username" json:"username"`
	Password    string `bson:"password" json:"password"`
	NamaLengkap string `bson:"nama_lengkap" json:"nama_lengkap"`
}

type Response struct {
	Code    int    `json:"code"`
	Message string `json:"message"`
}
