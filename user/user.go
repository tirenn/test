package user

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	c "test/config"

	"github.com/julienschmidt/httprouter"
	"golang.org/x/crypto/bcrypt"
)

var config *c.ConfigType

func Load(C *c.ConfigType) {
	config = C
}

func Create(w http.ResponseWriter, r *http.Request, params httprouter.Params) {
	var user User
	var output Response

	err := json.NewDecoder(r.Body).Decode(&user)
	if err != nil {
		log.Println("[Decode Request Body]", err)
	}

	hashedPassword, err := bcrypt.GenerateFromPassword([]byte(user.Password), 8)
	user.Password = string(hashedPassword)

	_, err = config.DB.Exec("INSERT INTO `users` (username, password, nama_lengkap) VALUES (?, ?, ?)", user.Username, user.Password, user.NamaLengkap)
	if err != nil {
		fmt.Println(err.Error())
		return
	}

	output.Code = 200
	output.Message = "User created"

	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(output)
	return

}

// func Delete(w http.ResponseWriter, r *http.Request, params httprouter.Params) {
// 	var user User
// 	var output Response
// 	id, _ := url.QueryUnescape(r.FormValue("id"))
// 	iduint, _ := strconv.ParseUint(id, 2, 32)
// 	user.ID = uint(iduint)
// 	db := sql.Connect()

// 	db.Delete(&user)

// 	output.Code = 200
// 	output.Message = "User Deleted"

// 	w.Header().Set("Content-Type", "application/json")
// 	json.NewEncoder(w).Encode(output)
// 	return

// }

// func Get(w http.ResponseWriter, r *http.Request, params httprouter.Params) {
// 	var user []User
// 	var output Response

// 	db := sql.Connect()
// 	db.Find(&user)

// 	output.Code = 200
// 	// output.Message = "User created"
// 	output.Data = user

// 	w.Header().Set("Content-Type", "application/json")
// 	json.NewEncoder(w).Encode(output)
// 	return

// }

// func GetbyId(w http.ResponseWriter, r *http.Request, params httprouter.Params) {
// 	var user User
// 	var output ResponseById

// 	id, _ := url.QueryUnescape(r.FormValue("id"))

// 	db := sql.Connect()
// 	db.Find(&user, id)

// 	output.Code = 200
// 	// output.Message = "User created"
// 	output.Data = user

// 	w.Header().Set("Content-Type", "application/json")
// 	json.NewEncoder(w).Encode(output)
// 	return

// }

// func Login(w http.ResponseWriter, r *http.Request, params httprouter.Params) {
// 	var user User
// 	var userParams User

// 	var output Response

// 	db := sql.Connect()

// 	err := json.NewDecoder(r.Body).Decode(&userParams)
// 	if err != nil {
// 		log.Println("[Decode Request Body]", err)
// 	}

// 	db.Where("username = ?", userParams.Username).Find(&user)

// 	log.Println(user.Password)
// 	log.Println(userParams.Password)
// 	hashedPassword, err := bcrypt.GenerateFromPassword([]byte(userParams.Password), 8)

// 	if err = bcrypt.CompareHashAndPassword([]byte(user.Password), []byte(string(hashedPassword))); err != nil {
// 		// If the two passwords don't match, return a 401 status
// 		w.WriteHeader(http.StatusUnauthorized)
// 		output.Code = 401
// 		output.Message = "Login Failed"

// 	} else {
// 		output.Code = 200
// 		output.Message = "Login Success"
// 	}

// 	w.Header().Set("Content-Type", "application/json")
// 	json.NewEncoder(w).Encode(output)
// 	return

// }
